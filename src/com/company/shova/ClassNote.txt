package cerotid;

class Animal {
	public void eat() {
		System.out.println("Eats through Mouth");
	}
}
class Lion extends Animal{

}
class Tiger extends Animal{

}
class Horse extends Animal{

}
public class AnimalTest {
	public static void main(String[] args) {
		// t is an object of Tiger
		// t is an instance of Tiger
		// t is an object variable
		// t is an object variable of type Tiger referring Tiger
		// class is a user defined data type
		Tiger t = new Tiger();
		t.eat();

		Lion l
Durga Maharjan8:50 PM
Object abc = new Tiger();     [ this is okay then???]
Saju Pappachen8:51 PM
package cerotid;

class Animal {
	public void eat() {
		System.out.println("Eats through Mouth");
	}
}
class Lion extends Animal{

}
class Tiger extends Animal{

}
class Horse extends Animal{

}
public class AnimalTest {
	public static void main(String[] args) {

		// a is an object variable of type Animal referring Tiger
		// The ability of an object variable of one type to refer different sub types is called dynamic polymorphism

		Animal a = new Tiger();
		a.eat();

		a = new Lion();
Saju Pappachen 9:48 PM
package cerotid;

// static polymorphism
// method overloading
// A class having more than one methods with same name is called method overloading
// Same operation in different ways for different data
public class AreaCalculator {
	public int getArea(int a, int b){
		return a * b;
	}
	public double getArea(int r){
		return 3.14 * r * r;
	}

}

class Mobile{
	private String color;
	private int price;

	// Constructor overloading

	public Mobile(){
	}
	public Mobile(String color){
		this.color = color;
	}
	public Mobile(int price){
		this.price = price;
	}

	public Mobile(String color, int price){
		this.color = color;
		this.price = price;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getPrice() {
		return price;
	}

	// constructor gets called once per object creation
    //whereas a method can be called any number of time
Remember if equals() method does not exist in a user supplied class then the inherited Object class’s equals() method is run which evaluates if the references point to the same object in memory. The object.equals() works just like the “==” operator (i.e shallow comparison).


Thread
package com.jthread.thread;

class MyThread extends Thread {
	@Override
	public void run() {
		for (int i = 1; i <= 5; i++) {
			System.out.println(i);
		}
	}
}

public class MyThreadTest {

	public static void main(String[] args) {
		MyThread mt = new MyThread();
		mt.start();
	}


}
package com.jthread.thread;

class YourThread implements Runnable{
	@Override
	public void run() {
		for (int i = 1; i <= 5; i++) {
			System.out.println(i);
		}
	}
}
public class YourThreadTest {

	public static void main(String[] args) {
		YourThread yt = new YourThread();
		Thread t = new Thread(yt);
		t.start();
	}

}
