package com.company.shova.day10;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("C:\\Users\\bikra\\IdeaProjects\\Training\\src\\com\\company\\shova\\fileRead.txt");
            int data= fis.read();
            while(data!=-1){
                System.out.print((char)data);
                data = fis.read();
            }
            System.out.println("Done");
        } catch (FileNotFoundException e) {
            System.out.println("file is not available");
        } catch (IOException e) {
            System.out.println("Cannot read");
        } finally {

            try {
                if (fis != null) {
                    fis.close();
                }
                System.out.println("Finally called");
            } catch (IOException e) {
                System.out.println("Some IOException while closing");
            }

        }
    }
}
