package com.company.shova.day10;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

//1 Create a Stream
//2 Write int the Stream
//closs it
//byte streeam or character stream
// outputStream- FileOutputStream
public class FileWrite {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream ("C:\\Users\\bikra\\IdeaProjects\\Training\\src\\com\\company\\shova\\fileWrite.txt");
            fos.write(("I love Java".getBytes()));
            System.out.println("hello");
        } catch (FileNotFoundException e) {
            System.out.println("not found");

        } catch (IOException e) {
            System.out.println("IO Exception while writing");
        }finally {
            try{
                if (fos != null) {
                    fos.close();
                }
            }catch (IOException e){
                System.out.println("IO Exception while closing");
            }

        }
    }
}
