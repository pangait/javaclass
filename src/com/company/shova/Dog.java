package com.company.shova;
// fully encapsulated class
// this refers the current object(the object in which you call method)
//getters amd setters
//identifiers=a name given to the package, class variables methods
//identifier rules and convention
//identifiers must start with alphabets,_ or $ after that it can be anything else(digits, alphabets, $, _) but not special character.
//class must start with capital letters
//methods and variables name must start with lowercase
//Camel case
//package name use reverse domain()
public class Dog {
    private String name;
    private int age;
    private String highestValue;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if (age > 40) {

            //do something
        } else {
            this.age = age;
        }

    }


    public void bark() {
        System.out.println("Dog bark.");
    }

}
