package com.company.shova;

class Mobile{
    private String color;
    private int price;

    // constructor
    // constructor does not have return type
    // constructor is used to initialized the variables
    // constructor overloading
    // a default no argument constructor will be added by the compiler
    //constructor is for instantiate the object

    public Mobile() {
        System.out.println("CALLED");
    }


    public Mobile(String color, int price) {
        this.color = null;
        this.price = 0;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
public class ConstructorTest {
    public static void main(String[] args) {
        Mobile m = new Mobile("Black", 250);
        m.setColor("Black");
        m.setPrice(250);
        System.out.println(m.getColor());//null
        System.out.println(m.getPrice());//0

        Mobile m1 = new Mobile();
        System.out.println(m.getColor());//null
        System.out.println(m.getPrice());
    }
}
