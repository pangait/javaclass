package com.company.shova;

class A {
    public void m1() {
        System.out.println("In m1");
    }
}
class B extends A{
    public void m1() {
        System.out.println("");
    }
    // public abstract void m2();
}
public class FinalDemo {
    public static void main(String[] args) {
        int value = 20;
        System.out.println(value);
        value = value+10;
        System.out.println(value);
        A a = new B();
        a.m1();
    }
}
