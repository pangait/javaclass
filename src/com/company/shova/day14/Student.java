package com.company.shova.day14;

public class Student {
    private String name;
    private int age;
    {
        System.out.println("Instance initializer block");
        System.out.println(this.name);
        System.out.println(this.age);
        this.name = "instance initializer name";
        this.age = 10;
    }

    public Student() {
        System.out.println("Constructor called.");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
