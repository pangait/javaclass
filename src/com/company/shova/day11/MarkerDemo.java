package com.company.shova.day11;
//Marker interface
interface Beautiful{

}
class Pen implements Beautiful{

}
public class MarkerDemo {
    public static void main(String[] args) {
        Pen pen = new Pen();
        System.out.println(pen instanceof Pen);
        System.out.println(pen instanceof Beautiful);
    }
}
