package com.company.shova.day11;

import java.io.*;

public class DeserializableCar {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        InputStream fis = new FileInputStream("C:\\Users\\bikra\\IdeaProjects\\Training\\src\\com\\company\\shova\\CarWrite.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);
        Car a = (Car) ois.readObject();
        System.out.println(a.getColor()+ " : " + a.getPrice());
    }
}
