package com.company.shova.day11;

import java.io.*;

public class PersonReadDemo {
    public static void main(String[] args) {
        InputStream inputStream = null;
        InputStream bufferedInputStream = null;
        try {
            inputStream = new FileInputStream("C:\\Users\\bikra\\IdeaProjects\\Training\\src\\com\\company\\shova\\fileRead.txt");
            bufferedInputStream = new BufferedInputStream(inputStream);
            int data = 0;
            data = inputStream.read();
            while (data != -1) {
                System.out.print((char) data);
                data = inputStream.read();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (bufferedInputStream != null) {
                    bufferedInputStream.close();
                }
            } catch(IOException e){
                e.printStackTrace();
            }
        }

    }
}
