package com.company.shova.day11;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileReadDemo {
    public static void main(String[] args) throws IOException {
        InputStream is = new FileInputStream("C:\\Users\\bikra\\IdeaProjects\\Training\\src\\com\\company\\shova\\fileRead.txt");
        BufferedInputStream bis = new BufferedInputStream(is);
        int data = is.read();
        while (data!=-1) {
            System.out.print((char) data);
            data = is.read();
        }
    }
}
