package com.company.shova.day11;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;

public class DeserializePerson {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        InputStream inputStream = new FileInputStream("C:\\Users\\bikra\\IdeaProjects\\Training\\src\\com\\company\\shova\\serPerson.txt");
        ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        Person person = (Person) objectInputStream.readObject();
        System.out.println(person.getName() + " : " + person.getAge());
    }
}
