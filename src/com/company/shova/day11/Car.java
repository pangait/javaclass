package com.company.shova.day11;

import java.io.Serializable;

public class Car implements Serializable {
    private static final long serialVersionUID = 7069563724313119012L;
    private String color;
    private int price;
    private String model;

    public Car(String color, int price) {
        this.color = color;
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
