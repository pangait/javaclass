package com.company.shova.day11;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

public class SerializablePerson {
    public static void main(String[] args) throws IOException {
        Person person = new Person("Ram", 25 );
        OutputStream fos = new FileOutputStream("C:\\Users\\bikra\\IdeaProjects\\Training\\src\\com\\company\\shova\\serPerson.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(person);
    }
}
