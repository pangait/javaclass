package com.company.shova;
abstract class Animal{
    public void eat(){
        System.out.println("Eat through Mouth.");
    }
    public abstract void travel();
}
class Lion extends Animal{
    public void travel(){
        System.out.println("Walks using legs");
    }

}
class Tiger extends Animal{
    public void travel(){
        System.out.println("Walks using legs");
    }

}
class Horse extends Animal {
    public void travel(){
        System.out.println("Walks using legs");
    }
}
class Parrot extends Animal{
    public void travel(){
        System.out.println("Fly using wings");
    }
}

public class AnimalTest {
    public static void main(String[] args) {
        // t is an instance of Tiger
        // t is an object of Tiger
        // t is an object variable
        // t is an object variable of type Tiger referring Tiger
        // class is a user define data type
        //int i = 10;
       // Tiger t = new Tiger();
        //Lion l = new Lion();
        //l.eat();
        //Horse h = new Horse();
        //h.eat();
        // a is an object variable of type Animal referring Tiger
        //The ability of an object variable of one type to refer different sub types is called dynamic polymorphism
        Animal a = new Tiger();
       // t.eat();
        a.eat();
        a.travel();
        a = new Lion();
        a = new Horse();
        a = new Parrot();
        a.eat();
        a.equals(a);
        a.travel();

    }

}
