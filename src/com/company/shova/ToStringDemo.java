package com.company.shova;

class Student {
    private  String name;

    public Student() {

    }
    public Student(String name){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //Method overriding
    @Override
    public String toString() {
        return "Hello to" + name +".";
    }

}
public class ToStringDemo {
    public static void main(String[] args) {
        Student s = new Student();
        s.setName("Sujit");
        System.out.println(s);
        System.out.println(s.toString());
        System.out.println(s.getName());
    }

}
