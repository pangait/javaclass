package com.company.shova.class7;

class Base {
    public Base() {
        System.out.println(10);
    }
    public Base(int a) {
        System.out.println(20);
    }
}

class Sub extends Base {
    public Sub() {
        //super calls the super class default constructor
        // super();
        this(10);
        System.out.println(30);
    }
    public Sub(int a) {
        //super calls the super class default constructor
       // super(10);
        super(10);

        System.out.println(40);
    }
}

class SubChild extends Sub {
    public SubChild(){
        System.out.println(50);
    }


}
public class ConstructorChaining {
    public static void main(String[] args) {
        Sub sub = new Sub();
        System.out.println();
        SubChild subChild = new SubChild();
    }
}
