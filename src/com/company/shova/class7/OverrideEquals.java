package com.company.shova.class7;
class Bus{
    private String color;
    private String make;

    public Bus(String color, String make) {
        super();
        this.color = color;
        this.make = make;
    }
//    @Override
//    public boolean equals(Object obj) {
//        // Downcasting
//        Bus b = (Bus) obj;
//        if (this.color.equals(b.color) && this.make.equals(b.make)){
//            return true;
//        }else {
//            return false;
//        }
//    }
}
public class OverrideEquals {
    public static void main(String[] args) {
        // There is only one object here
        Bus b1 = new Bus("Red", "Honda");
        Bus b2 = new Bus("Red", "Honda");

        //Bus b2 = b1;

        System.out.println(b1.equals(b2));
        System.out.println(b1==b2);

    }
}
