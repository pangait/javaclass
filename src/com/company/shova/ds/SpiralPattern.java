package com.company.shova.ds;

public class SpiralPattern {

    public static void main(String[] args) {

        int n = 4;
        int[][] spiralMatrix = new int[n][n];
        char direction = 'r';
        int rowIndex = 0;
        int colIndex = 0;

        int straightLimit = n - 1;
        int nextStraightCount = n - 1;
        boolean firstTime = true;

        for (int i = 1; i <= n*n ; i++) {
            spiralMatrix[rowIndex][colIndex] = i;
            if (direction == 'r') {
                colIndex++;
            } else if (direction == 'd'){
                rowIndex++;
            }else if (direction =='l'){
                colIndex--;
            }else if (direction == 'u'){
                rowIndex--;
            }

            if(i == straightLimit) {
                straightLimit += nextStraightCount;
                if (firstTime) {
                    firstTime = false;
                } else {
                    nextStraightCount--;
                    firstTime = true;
                }
                if (direction == 'r') {
                    direction = 'd';
                } else if (direction == 'd'){
                    direction = 'l';
                }else if (direction =='l'){
                    direction = 'u';
                }else if (direction == 'u'){
                    direction = 'r';
                }
            }
        }

        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(spiralMatrix[i][j] +"\t");
            }
            System.out.println();
        }
    }
}
