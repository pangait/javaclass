package com.company.shova.ds;

public class MergeArrays {
    public static void main(String[] args) {

        /**
         * expected output {1,2,3,4,4,5,6,8}
         */
        //int[] arr1 = {6, 4, 2, 8};
        int[] arr1 = {2, 4, 6, 8};
        int[] arr2 = {1, 3, 4, 5};
        int[] arr3 = sortAndConcat(arr1, arr2);
        for (int i = 0; i < arr3.length; i++) {
            System.out.print(arr3[i] + "\t");
        }

        System.out.println();
        /**
         * expected output {4,5,7,8,9}
         */
        //int[] arr11 = {5,8,9};
        int[] arr11 = {8,5,9};
        int[] arr12 = {4,7,8};

        int[] arr4 = sortAndMerge(arr11, arr12);
        for (int i = 0; i <= arr4[arr4.length - 1]; i++) {
            System.out.print(arr4[i] + "\t");
        }
    }

    private static int[] sortAndMerge(int[] arr1, int[] arr2) {
        sort(arr1);
        sort(arr2);
        int[] result= new int[arr1.length + arr2.length + 1];
        int secondArrayIndex = 0;
        int resultIndex = -1;
        boolean arr1Done = false;
        for (int i = 0; i < arr1.length; i++) {
            if (secondArrayIndex == arr2.length || arr1[i] <= arr2[secondArrayIndex]) {
                resultIndex = setResult(result, resultIndex, arr1[i]);
                continue;
            }
            for (; secondArrayIndex < arr2.length; secondArrayIndex++) {
                if (arr1[i] >= arr2[secondArrayIndex] || arr1Done) {
                    resultIndex = setResult(result, resultIndex, arr2[secondArrayIndex]);
                    if (!arr1Done && secondArrayIndex == arr2.length - 1) {
                        resultIndex = setResult(result, resultIndex, arr1[i]);
                    }
                    continue;
                } else {
                    resultIndex = setResult(result, resultIndex, arr1[i]);
                    if (i == arr1.length - 1) {
                        arr1Done = true;
                        resultIndex = setResult(result, resultIndex, arr2[secondArrayIndex]);
                        continue;
                    }
                    break;
                }
            }
        }
        result[result.length - 1] = resultIndex;
        return result;
    }

    private static int setResult(int[] result, int currentResultIndex, int value) {
        if (currentResultIndex >=0) {
            if (result[currentResultIndex] != value)
                result[++currentResultIndex] = value;
        } else {
            result[++currentResultIndex] = value;
        }
        return currentResultIndex;
    }

    private static int[] sortAndConcat(int[] arr1, int[] arr2) {
        sort(arr1);
        sort(arr2);
        int[] result= new int[arr1.length + arr2.length];
        int secondArrayIndex = 0;
        int resultIndex = -1;
        boolean arr1Done = false;
        for (int i = 0; i < arr1.length; i++) {
           if (secondArrayIndex == arr2.length || arr1[i] <= arr2[secondArrayIndex]) {
               result[++resultIndex] = arr1[i];
               continue;
           }
            for (; secondArrayIndex < arr2.length; secondArrayIndex++) {
                if (arr1[i] >= arr2[secondArrayIndex] || arr1Done) {
                    result[++resultIndex] = arr2[secondArrayIndex];
                    if (!arr1Done && secondArrayIndex == arr2.length - 1) {
                        result[++resultIndex] = arr1[i];
                    }
                    continue;
                } else {
                    result[++resultIndex] = arr1[i];
                    if (i == arr1.length - 1) {
                        arr1Done = true;
                        result[++resultIndex] = arr2[secondArrayIndex];
                        continue;
                    }
                    break;
                }
            }
        }
        return result;
    }

    private static void sort(int[] arr) {
        final int length = arr.length;
        for (int i = 0; i < length - 1 ; i++) {
            for (int j = i + 1; j < length ; j++) {
                if (arr[i] > arr[j]) {
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
    }
}
