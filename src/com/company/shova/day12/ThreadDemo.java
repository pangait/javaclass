package com.company.shova.day12;
/*class A extends Thread{
    @Override
    public void run() {
        for (int i = 1; i <=1000; i++) {
            System.out.println(i);
        }
    }
}*/
//Ready to Run -> Running -> dead
          // sleeping, waiting, blocked
class A implements Runnable{
    @Override
    public void run() {
        for (int i = 1; i <=1000; i++) {
            System.out.println("A " + i);
        }
    }
}
class B implements Runnable{
    @Override
    public void run() {
        for (int i = 1; i <=1000; i++) {
            System.out.println("B " + i);
        }
    }
}
public class ThreadDemo {
    public static void main(String[] args) {
        A a = new A();
        Thread t1 = new Thread(a);
        B b = new B();
        Thread t2 = new Thread(b);
        t1.start();
        t2.start();
        for (int i = 1; i <=50; i++) {
            System.out.println("main" + i);
        }
        System.out.println("Done");
    }
}
