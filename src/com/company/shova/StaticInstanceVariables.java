package com.company.shova;

class Pen {
// indtance variable
    private int price;
    //static variable
    static String color = "blue";

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

}
public class StaticInstanceVariables {
    public static void main(String[] args) {
        Pen p = new Pen();
        p.setPrice(10);

        Pen p1 = new Pen();
        p1.setPrice(15);

        System.out.println(p.getPrice());
        System.out.println(p1.getPrice());
        System.out.println(p.color);
        System.out.println(p1.color);
        System.out.println(Pen.color);

    }

}
