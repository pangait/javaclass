package com.company.shova.day8;

interface Sample {
    int a =10;
    public void m1();
}

class A implements Sample {

    @Override
    public void m1() {
        System.out.println("Do something");
    }
}

public class InterfacePractice {
    public static void main(String[] args) {

    }
}
