package com.company.shova.day8;
// Tested and proven solution for a general problem is called Design Pattern
// Eg: SingleTon
class  FootBall{
    private static FootBall football = new FootBall();
    private  FootBall(){

    }

    public static FootBall getFootball() {
        return football;
    }
}
public class SingleTonDemo {
    public static void main(String[] args) {
        FootBall ft1 =FootBall.getFootball();
        FootBall ft2 =FootBall.getFootball();
        FootBall ft3 =FootBall.getFootball();
        System.out.println(ft1);
        System.out.println(ft2);
        System.out.println(ft3);

    }

}
