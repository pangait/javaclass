package com.company.shova;
// static polymorphism
public class AreaCalculator {
    public int getAreaOfRectangle(int a, int b){
        return a*b;
    }
    public double getAreaOfCircle(float r){
        return 3.14 * r*r;
    }

}
