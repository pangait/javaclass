package com.company.shova.lambda;

import java.util.function.Predicate;

/*class Rocket implements IRocket{
    public void fly () {
        System.out.println("Rocket is Flying.");
    }
}*/
class Animal {
    String name;
    boolean canFly;

    public Animal(String name, boolean canFly) {
        this.name = name;
        this.canFly = canFly;
    }
}

@FunctionalInterface
interface IRocket {
    void fly(String im);
}

@FunctionalInterface
interface IPre {
    boolean checkIfCanFly(Animal a);
}

class FlyChecker implements IPre {

    @Override
    public boolean checkIfCanFly(Animal a) {
        return a.canFly;
    }
}

public class LambdaPractice {

    public static void main(String[] args) {
        //Rocket rocket = new Rocket();
        whatItDoes(i -> System.out.println(i + " is flying"), "Man");
        whatItDoes(i -> System.out.println(i + " is flying"), "Plane");
        Animal a1 = new Animal("Bird", true);
        Animal a2 = new Animal("Cow", false);

        String s = "<table border=\"1\" >\n" +
                "  <caption><h1>Student Managment</h1></caption>\n" +
                "  <tr>\n" +
                "    <th>Student ID</th>\n" +
                "    <th>First Name</th>\n" +
                "    <th>Middle Name</th>\n" +
                "    <th>Last Name</th>\n" +
                "    <th>Cell No</th>\n" +
                "    <th>Address</th>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td>1</td>\n" +
                "    <td>First Name</td>\n" +
                "    <td>Middle Name</td>\n" +
                "    <td>Last Name</td>\n" +
                "    <td>Cell No</td>\n" +
                "    <td>Address of student </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td>\n" +
                "      <span></span>\n" +
                "      <span style=\"font-size: 55px\">Hello world!And evrething and spaces!.....</span>\n" +
                "    </td>\n" +
                "  </tr>\n" +
                "\n" +
                "  <tr>\n" +
                "    <td>2</td>\n" +
                "    <td>First Name</td>\n" +
                "    <td>Middle Name</td>\n" +
                "    <td>Last Name</td>\n" +
                "    <td>Cell No</td>\n" +
                "    <td>Address of student 2 </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td>3</td>\n" +
                "    <td>First Name of your's first Name of mine.</td>\n" +
                "    <td>First Name</td>\n" +
                "    <td>Middle Name of your's middle name of mine</td>\n" +
                "    <td>Last Name of </td>\n" +
                "    <td>Cell No</td>\n" +
                "    <td>Address of student 3 </td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td>4</td>\n" +
                "    <td>First Name 4</td>\n" +
                "    <td>Middle Name</td>\n" +
                "    <td>Last Name</td>\n" +
                "    <td>Cell No</td>\n" +
                "    <td>Address of student 4 </td>\n" +
                "  </tr>\n" +
                "</table>";


        // ---Using Object implementing interface -------------
        FlyChecker flyChecker = new FlyChecker();
        animalCanFly(flyChecker, a2);

        //----Using Lambda ----------
        animalCanFly(a -> a.canFly, a1);

        //-----Using Predicate-----------
        animalCanFlyUsingPredicate(a -> a.canFly, a1);

    }

    private static void animalCanFlyUsingPredicate(Predicate<Animal> animalPredicate, Animal animal) {
        if (animalPredicate.test(animal)) {
            System.out.println(animal.name + " can fly.");
        } else {
            System.out.println(animal.name + " can not fly.");
        }
    }

    private static void animalCanFly(IPre pre, Animal animal) {
        if (pre.checkIfCanFly(animal)) {
            System.out.println(animal.name + " can fly.");
        } else {
            System.out.println(animal.name + " can not fly.");
        }
    }

    private static void whatItDoes(IRocket rocket, String item) {
        rocket.fly(item);
    }
}
