package com.company.shova.day9;

import java.io.FileNotFoundException;

public class Test {
    public static void main(String[] args) {
        int arr[] = {10, 20, 30};
        System.out.println(arr[0]);
        System.out.println(arr[1]);
        System.out.println(arr[2]);
        System.out.println(arr[3]);
        /*System.out.println(10/0);*/
        try{
                //Opening a stream
                //..
                //..
                //.close the stream
                /*try {
                    //..
                }catch (FileNotFoundException e) {

                    //..
                }catch()
                //..*/
            }catch(Exception e){
                //code to recover from that
                try{
                    //Opening a stream
                    //..
                    //..
                    //.close the stream
                    /*try {
                        //..
                    }catch (FileNotFoundException e) {

                        //..
                    }catch()
                    //..*/
                }catch(RuntimeException a){
                    //code to recover from that
                }
            }finally {
            //..
        }

    }
    public void doSomething() throws Exception {

        if (true) {
        }
    }
}
