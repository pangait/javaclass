package com.company.shova.day9;
//byte-> short int long float double
public class PrimitiveCasting {
    public static void main(String[] args) {
        byte b = 29;
        int i = b; // implicit casting
        System.out.println(i);
        int a = 20;
        byte bi = (byte) a; //explicit casting
        System.out.println(bi);
        float f = 3.5f;
        float f1 = (float) 3.5;
        System.out.println(f);
    }
}
