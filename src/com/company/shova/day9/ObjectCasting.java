package com.company.shova.day9;
class Vehicle{

}

class Bus extends Vehicle {

}
class Car extends Vehicle {

}
class Toyota extends Car {

}
public class ObjectCasting {
    public static void main(String[] args) {
        //v1 type is Vehicle and it is referring Vehicle
        Vehicle v1 = new Vehicle();
        //v2 type is Vehicle and it is referring Car
        Vehicle v2 = new Car();//upcasting or implicit casting
        //Car c0 = v2;  // Downcasting or Explicit Casting
        Car c1 = (Car)v1;  // Downcasting or Explicit Casting
        //Car c2 = new Bus();  // Downcasting or Explicit Casting
        Car c3 = new  Toyota();
        System.out.println("Done");
    }
}
