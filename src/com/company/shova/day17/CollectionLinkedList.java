package com.company.shova.day17;

import java.util.LinkedList;

public class CollectionLinkedList {
    public static void main(String[] args) {

        LinkedList linkedList = new LinkedList();

        Student student1 = new Student();
        student1.setStudentId(1);
        student1.setFirstName("Ram");
        student1.setMiddleName("Bahadur");
        student1.setLastName("Singh");
        student1.setPhoneNo(123567);

        linkedList.add(student1);
        System.out.println();
    }
}
