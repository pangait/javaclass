package com.company.shova.day17;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class CollectionHashMap {
    public static void main(String[] args) {
        HashMap<Integer, Student> studentMap = new HashMap<>();

        Student student1 = new Student();
        student1.setStudentId(1);
        student1.setFirstName("Ram");
        student1.setMiddleName("Bahadur");
        student1.setLastName("Singh");
        student1.setPhoneNo(123567);

        studentMap.put(student1.getStudentId(), student1);

        Iterator keyIterator = studentMap.keySet().iterator();
        while (keyIterator.hasNext()){
            Integer studentId = (Integer) keyIterator.next();
            Student student = (Student) studentMap.get(studentId);
            /*System.out.println(student.getFirstName()+ " " + student.getMiddleName() + " " + student.getLastName());
            System.out.println(student.getPhoneNo());
            System.out.println(student.getStudentId());*/
            System.out.println(student);

        }
    }
}
