package com.company.shova.day17;

import java.util.ArrayList;

public class CollectionArrayList {
    public static void main(String[] args) {

        ArrayList<Student> students = new ArrayList<>();

        Student student1 = new Student();
        student1.setStudentId(1);
        student1.setFirstName("Ram");
        student1.setMiddleName("Bahadur");
        student1.setLastName("Singh");
        student1.setPhoneNo(123567);

        students.add(student1);

        Student student = students.get(0);
        System.out.println("Student Name : ");
    }
}
