package com.company.shova;

public class DogMain {
    public static void main(String[] args) {
        //d is an object
        //d is an instance
        //d is an object
        //d is an object variable of type Dog
        Dog d = new Dog();
        d.setName("Jimmy");
        d.setAge(2);
        System.out.println(d.getName());
        System.out.println(d.getAge());
        System.out.println(d);
        Dog d1 = d;
        d.bark();

     /*   Dog d1 = new Dog();
        d1.name ="Blacky";
        d1.age = 3;
        System.out.println(d1.name);
        System.out.println(d1.age);
        System.out.println(d1);*/


    }
}
