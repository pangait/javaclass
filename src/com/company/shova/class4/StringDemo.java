package com.company.shova.class4;
//String belongs to java.lang package
public class StringDemo {
    public static void main(String[] args) {
        String name = "Monica";
        String name1 = new String("Monica");
        String name2 = new String("Monica");

        //String name1 = "Monica";
        String state = new String("Texas");
        System.out.println(name);
       // name = name +" Lama";
        System.out.println(name==name1);
        System.out.println(name.equals("Monica"));
        System.out.println(name.length());
        System.out.println(name.toUpperCase());
        System.out.println(name.charAt(1));

    }
}
