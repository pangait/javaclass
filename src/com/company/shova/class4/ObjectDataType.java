package com.company.shova.class4;
class Student{
    private String name;

    public Student() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

public class ObjectDataType {
    public static void main(String[] args) {
        int i = 0;
        Student s = new Student();

    }
}
