package com.company.shova.class4;

public class ArrayDemo {
    public static void main(String[] args) {
        int mark[];//declaration
        mark = new int[3];// creation
        mark[0]=75;
        mark[1]=80;
        mark[2]=70;
        int marks[] ={75, 80, 70};
        System.out.println(mark[0]);
        int a[][] = new int[2][];
        a[0] = new int[2];
        a[1] = new int[3];
        int b[][] = {{10,20},{30,40,50}};
    }
}
