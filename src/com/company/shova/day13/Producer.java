package com.company.shova.day13;

public class Producer extends Thread {
   private Holder holder;
    public Producer(Holder holder)  {
        super();
        this.holder = holder;
    }

    @Override
    public void run() {
        for (int i = 0; i <= 3; i++) {
               holder.setData(i);
        }
    }
}
