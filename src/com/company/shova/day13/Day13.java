package com.company.shova.day13;

public class Day13 {
    public static void main(String[] args) {
        Holder holder = new Holder();
        Producer producer = new Producer(holder);
        Consumer consumer = new Consumer(holder);
        producer.start();
        consumer.start();
    }
}
