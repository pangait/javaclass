package com.company.shova.day13;

public class Holder {
    private int data;
    boolean flag = false;

    public synchronized int getData() {
        if (flag == false) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Consumer :" + data);
        notify();
        flag = false;
        return data;
    }

    public synchronized void setData(int data) {
        if (flag == true){
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Producing :" + data);
        notify();
        flag = true;
        this.data = data;
    }
}
